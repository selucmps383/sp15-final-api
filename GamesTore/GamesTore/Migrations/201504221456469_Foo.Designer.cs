// <auto-generated />
namespace GamesTore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class Foo : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Foo));
        
        string IMigrationMetadata.Id
        {
            get { return "201504221456469_Foo"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
